"""
Recorder
"""
from flask import request
from flask.views import MethodView
from flask_smorest import Blueprint, abort

blp = Blueprint("recorder", __name__, description="Recording audio")


@blp.route("/recorder")
class Recorder(MethodView):
    def get(self):
        return 'done with GET'

    def post(self):
        audio_data = request.files['audio_data'].read()
        return audio_data


