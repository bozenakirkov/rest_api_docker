from flask import url_for

from app import create_app


def test_app():

    with create_app().test_request_context():
        assert url_for("stores.Store", store_id=1) == "/store/1"
        assert url_for("stores.StoreList") == "/store"
        assert url_for('items.Item', item_id=1) == "/item/1"
        assert url_for('items.ItemList') == "/item"


